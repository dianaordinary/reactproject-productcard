import React, {useState} from "react";
import Title from "../title/title";
import Code from "../code/code";
import Description from "../description/description";
import Comments from "../comments/comments";
import Popularity from "../popularity/popularity";
import {Image} from "../elements";
import Tabs from "../tabs/tabs";
import {
    StyledProductPage,
    Header,
    ProductWrapper,
    ProductInfo,
    ProductInfoLine,
    PageCounter,
    BuyButton,
    PageFullPrice,
    DeliveryValue
} from "./styled";
import PopUp from "../popup/popup";
import Order from "../order/order";
import Accordion from "../accordion/accordion";
import Slider from "../slider/slider";

const MAX_TEXT_SIZE = 200;
const COMMENTS_COUNT = 3;

function ProductPage({ product, showInfoInAccordion }) {
    //состояние, хранящее выбранное количество товаров,
    // и передать его в компонент счётчика через пропс вместе с сеттером состояния.
    const [productCount, setProductCount] = useState(1);
    const price = product.price * productCount;
    const oldPrice = product.oldPrice * productCount;
    const [isShowPopup, setIsShowPopup] = useState(false);
    // состояние, хранящее флаг показа описания товара целиком
    const [isShowAllDescription, setIsShowAllDescription] = useState(false);
    //  количество комментариев, которое нужно показать
    const [commentsShow, setCommentsShow] = useState(COMMENTS_COUNT);

    const tabs = [
        {
            title: "Описание",
            content: (
                <Description
                    text={
                        isShowAllDescription
                            ? product.description
                            : product.description.slice(0, MAX_TEXT_SIZE)
                    }
                    onShowMore={() => setIsShowAllDescription(!isShowAllDescription)}
                    isShowAllDescription={isShowAllDescription}
                />
            )
        },
        {
            title: "Комментарии",
            content: (<Comments
                comments={product.comments.slice(0,commentsShow)}
                onShowMore={()=> setCommentsShow(commentsShow + COMMENTS_COUNT)}
                allCommentsLength={product.comments.length}
            />)
        }
    ];

    return (
        <StyledProductPage>
            <Header>
                <Title>{product.name}</Title>
                <Code>{product.code}</Code>
            </Header>
            <ProductWrapper>
                <Slider images={product.images} />
                <ProductInfo>
                    <ProductInfoLine>
                        Цена: <PageFullPrice oldPrice={oldPrice} price={price} />
                    </ProductInfoLine>
                    <ProductInfoLine>
                        Количество:{" "}
                        <PageCounter
                            value={productCount}
                            minValue={1}
                            onChange={setProductCount}
                        />
                    </ProductInfoLine>
                    <ProductInfoLine>
                        <span>Доставка:</span>{" "}
                        <DeliveryValue>{product.delivery}</DeliveryValue>
                    </ProductInfoLine>
                    <BuyButton size="large" onClick={() => setIsShowPopup(true)}>
                        Купить
                    </BuyButton>
                    <Popularity count={product.comments.length} />
                </ProductInfo>
            </ProductWrapper>
            {showInfoInAccordion ? <Accordion items={tabs} /> : <Tabs tabs={tabs} />}
            <PopUp
                isShow={isShowPopup}
                onClose={() => setIsShowPopup(false)}
                title="Оформление"
            >
                <Order />
            </PopUp>
        </StyledProductPage>
    );
}

export default ProductPage;
