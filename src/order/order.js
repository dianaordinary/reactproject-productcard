import React, {useState} from "react";
import Button from "../button/button";
import {Label} from "./styled";

function Order() {
    // состояниея для каждого поля
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");

    // Проверка на заполнение полей
    const isButtonEnable = name && phone && address;

    const onChange = (evt, setChange) => setChange(evt.target.value);
    let onClick = (evt) => {
        evt.preventDefault();
        console.log(
            `${name}, спасибо за заказ. Мы доставим его как можно скорее по адресу: ${address}`
        );
    }

    return (
        <form>
            <Label>
                Имя:{" "}
                <input
                    name="name"
                    value={name}
                    onChange={(e) => onChange(e, setName)}/>
            </Label>
            <Label>
                Телефон:{" "}
                <input
                    name="phone"
                    type="tel"
                    value={phone}
                    onChange={(e) => onChange(e, setPhone)}/>
            </Label>
            <Label>
                Адрес доставки:{" "}
                <input
                    name="address"
                    value={address}
                    onChange={(e) => onChange(e, setAddress)}/>
            </Label>
            <Button
                disabled={!isButtonEnable}
                onClick={onClick}>
                Оформить
            </Button>
        </form>
    );
}

export default Order;
