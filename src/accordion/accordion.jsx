import React, {useEffect, useRef, useState} from "react";
import {Content, TitleButton, Wrapper, ContentWrapper} from "./styled";

function Accordion({items}) {
    // состояние, которое хранит выбранный индекс открытого элемента аккордеона
    const [activeIndex, setActiveIndex] = useState(0);
    //высота контента активного элемента
    const [height, setHeight] = useState(0);
    // ссылка, которая связана с элементом Content открытого элемента
    const openContent = useRef(null);
    // эффект, кот. сеттить состояние высоты высотой открытого элемента
    // когда массив элементов меняется, высота пересчитывается
    useEffect(() => {
        setHeight(openContent.current.offsetHeight);
    }, [height, activeIndex, items]);
    return (
        <div>
            {items && items.length &&
                items.map((item, index) =>
                    index === activeIndex ? (
                        //открытый элемент
                        <Wrapper key={item.title}>
                            <TitleButton as="span" isActive>
                                {item.title}
                            </TitleButton>
                            <ContentWrapper style={{height}}>
                                <Content ref={openContent}>{item.content}</Content>
                            </ContentWrapper>
                        </Wrapper>
                    ) : (
                        //закрытый элемент
                        <Wrapper key={item.title}>
                            <TitleButton onClick={() => setActiveIndex(index)}>
                                {item.title}
                            </TitleButton>
                            <ContentWrapper>
                                <Content>{item.content}</Content>
                            </ContentWrapper>
                        </Wrapper>
                    )
                )}
        </div>
    );
}

export default Accordion;
